#Author: agarciab@choucairtesting.com
#language:es

@regresion
Característica: Gestionar cita médica
  Como paciente
  Quiero realizar la solicitud de una cita médica
  A través del sistema de Administración de Hospitales

@casoexitoso1
  Escenario: Realizar el Registro de un Doctor
    Dado que Carlos necesita registrar un nuevo doctor
    Cuando el realiza el registro del mismo en el aplicativo de Administración de Hospitales
    |Nombres		|Apellidos		|Telefono	|Tipo documento			|Numero documento	|
    |Alexander		|Garcia Berrio	|4600700	|Cédula de ciudadanía	|1012		|
    Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente
    
@casoexitoso2
  Escenario: Realizar el Registro de un Paciente
    Dado que Carlos necesita registrar un nuevo paciente
    Cuando el realiza el registro del paciente en el aplicativo de Administración de Hospitales
    |Nombres		|Apellidos		|Telefono	|Tipo documento			|Numero documento	|
    |Paula			|Cañaveral		|4600800	|Cédula de ciudadanía	|2022		|
    Entonces el verifica que se almacene el paciente cuando se presente en pantalla el mensaje Datos guardados correctamente

@casoexitoso3
  Escenario: Realizar el Agendamiento de una Cita
    Dado que Carlos necesita asistir al medico
    Cuando el realiza el agendamiento de una Cita
    |Fecha cita		|Documento identidad Paciente	|Documento identidad Doctor	|Observaciones		|
    |06/09/2018		|2022													|1012												|Reto 3						|
    Entonces el verifica que se agende la cita presentando en pantalla el mensaje Datos guardados correctamente