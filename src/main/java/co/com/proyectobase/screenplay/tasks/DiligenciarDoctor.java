package co.com.proyectobase.screenplay.tasks;



import java.util.List;

import org.openqa.selenium.JavascriptExecutor;

import co.com.proyectobase.screenplay.interactions.Seleccionar;
import co.com.proyectobase.screenplay.ui.HospitalAgregarDoctorPage;
import co.com.proyectobase.screenplay.ui.HospitalMenuPage;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;



public class DiligenciarDoctor implements Task {
	
	private List<List<String>> datos;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(HospitalMenuPage.BOTON_AGREGAR_DOCTOR));
		
		actor.attemptsTo(Enter.theValue(datos.get(1).get(0).trim()).into(HospitalAgregarDoctorPage.CAMPO_NOMBRES_COMPLETOS_DOCTOR));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(1).trim()).into(HospitalAgregarDoctorPage.CAMPO_APELLIDOS_DOCTOR));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(2).trim()).into(HospitalAgregarDoctorPage.CAMPO_TELEFONO_DOCTOR));
		
		actor.attemptsTo(Seleccionar.laLista(HospitalAgregarDoctorPage.LISTA_TIPO_DOCUMENTO_IDENTIDAD_DOCTOR, datos.get(1).get(3).trim()));
		
		
		actor.attemptsTo(Enter.theValue(datos.get(1).get(4).trim()).into(HospitalAgregarDoctorPage.CAMPO_NUMERO_DOCUMENTO_IDENTIDAD_DOCTOR));
		
		actor.attemptsTo(Click.on(HospitalAgregarDoctorPage.BOTON_GUARDAR_DOCTOR));
		
		
	
	}
	
	

	public DiligenciarDoctor(List<List<String>> datos) {
		this.datos = datos;
	}


	public static DiligenciarDoctor FormularioDeRegistro(List<List<String>> datos) {
		return Tasks.instrumented(DiligenciarDoctor.class, datos);
	}

}
