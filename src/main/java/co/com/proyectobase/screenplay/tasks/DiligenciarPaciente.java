package co.com.proyectobase.screenplay.tasks;



import java.util.List;

import org.openqa.selenium.JavascriptExecutor;

import co.com.proyectobase.screenplay.interactions.Seleccionar;
import co.com.proyectobase.screenplay.ui.HospitalAgregarDoctorPage;
import co.com.proyectobase.screenplay.ui.HospitalAgregarPacientePage;
import co.com.proyectobase.screenplay.ui.HospitalMenuPage;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;



public class DiligenciarPaciente implements Task {
	
	private List<List<String>> datos;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
	
		actor.attemptsTo(Click.on(HospitalMenuPage.BOTON_AGREGAR_PACIENTE));
		
		actor.attemptsTo(Enter.theValue(datos.get(1).get(0).trim()).into(HospitalAgregarPacientePage.CAMPO_NOMBRES_COMPLETOS_PACIENTE));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(1).trim()).into(HospitalAgregarPacientePage.CAMPO_APELLIDOS_PACIENTE));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(2).trim()).into(HospitalAgregarPacientePage.CAMPO_TELEFONO_PACIENTE));
		
		actor.attemptsTo(Seleccionar.laLista(HospitalAgregarPacientePage.LISTA_TIPO_DOCUMENTO_IDENTIDAD_PACIENTE, datos.get(1).get(3).trim()));
		
		
		actor.attemptsTo(Enter.theValue(datos.get(1).get(4).trim()).into(HospitalAgregarPacientePage.CAMPO_NUMERO_DOCUMENTO_IDENTIDAD_PACIENTE));
		
		actor.attemptsTo(Click.on(HospitalAgregarPacientePage.BOTON_GUARDAR_PACIENTE));
		

	}
	
	

	public DiligenciarPaciente(List<List<String>> datos) {
		this.datos = datos;
	}


	public static DiligenciarPaciente FormularioDeRegistro(List<List<String>> datos) {
		return Tasks.instrumented(DiligenciarPaciente.class, datos);
	}

}
