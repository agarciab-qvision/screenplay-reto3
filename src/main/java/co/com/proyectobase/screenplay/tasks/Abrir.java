package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.HospitalMenuPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task {

	private HospitalMenuPage hospitalMenuPage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(hospitalMenuPage));
	
	}
	
	public static Abrir LaPaginaDelHospital() {
		
		return Tasks.instrumented(Abrir.class);
	}

}

