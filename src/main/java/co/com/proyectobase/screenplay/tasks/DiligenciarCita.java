package co.com.proyectobase.screenplay.tasks;



import java.util.List;

import org.openqa.selenium.JavascriptExecutor;

import co.com.proyectobase.screenplay.interactions.Seleccionar;
import co.com.proyectobase.screenplay.ui.HospitalAgendarCitaPage;
import co.com.proyectobase.screenplay.ui.HospitalAgregarDoctorPage;
import co.com.proyectobase.screenplay.ui.HospitalAgregarPacientePage;
import co.com.proyectobase.screenplay.ui.HospitalMenuPage;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;



public class DiligenciarCita implements Task {
	
	private List<List<String>> datos;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
	
		actor.attemptsTo(Click.on(HospitalMenuPage.BOTON_AGENDAR_CITA));
		
		actor.attemptsTo(Enter.theValue(datos.get(1).get(0).trim()).into(HospitalAgendarCitaPage.LISTA_DIA_CITA));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(1).trim()).into(HospitalAgendarCitaPage.CAMPO_NUMERO_DOCUMENTO_IDENTIDAD_PACIENTE));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(2).trim()).into(HospitalAgendarCitaPage.CAMPO_NUMERO_DOCUMENTO_IDENTIDAD_DOCTOR));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(3).trim()).into(HospitalAgendarCitaPage.CAMPO_OBSERVACIONES));
		
		actor.attemptsTo(Click.on(HospitalAgendarCitaPage.BOTON_GUARDAR_CITA));
		

	}
	
	

	public DiligenciarCita(List<List<String>> datos) {
		this.datos = datos;
	}


	public static DiligenciarCita FormularioDeRegistro(List<List<String>> datos) {
		return Tasks.instrumented(DiligenciarCita.class, datos);
	}

}
