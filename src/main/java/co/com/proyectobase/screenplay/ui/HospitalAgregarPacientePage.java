package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class HospitalAgregarPacientePage {

	public static final Target CAMPO_NOMBRES_COMPLETOS_PACIENTE = Target.the("El campo nombres completos del paciente").located(By.name("name"));
	public static final Target CAMPO_APELLIDOS_PACIENTE = Target.the("El campo apellidos del paciente").located(By.name("last_name"));
	public static final Target CAMPO_TELEFONO_PACIENTE = Target.the("El campo teléfono del paciente").located(By.name("telephone"));
	public static final Target LISTA_TIPO_DOCUMENTO_IDENTIDAD_PACIENTE = Target.the("La lista tipo de documento de identidad del paciente").located(By.name("identification_type"));
	public static final Target CAMPO_NUMERO_DOCUMENTO_IDENTIDAD_PACIENTE = Target.the("El campo número de documento de identidad del paciente").located(By.name("identification"));
	public static final Target BOTON_GUARDAR_PACIENTE = Target.the("El botón guardar datos del paciente").located(By.xpath("//*[@id='page-wrapper']/div/div[3]/div/a"));	
}
