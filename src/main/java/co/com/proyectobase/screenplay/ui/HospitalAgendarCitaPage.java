package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class HospitalAgendarCitaPage {

	public static final Target LISTA_DIA_CITA = Target.the("La lista día de la cita").located(By.id("datepicker"));
	public static final Target CAMPO_NUMERO_DOCUMENTO_IDENTIDAD_PACIENTE = Target.the("El campo número de documento de identidad del paciente").located(By.xpath("//*[@id='page-wrapper']/div/div[3]/div/div[2]/input"));
	public static final Target CAMPO_NUMERO_DOCUMENTO_IDENTIDAD_DOCTOR = Target.the("El campo número de documento de identidad del doctor").located(By.xpath("//*[@id='page-wrapper']/div/div[3]/div/div[3]/input"));
	public static final Target CAMPO_OBSERVACIONES = Target.the("El campo observaciones de la cita").located(By.xpath("//*[@id='page-wrapper']/div/div[3]/div/div[4]/textarea"));
	public static final Target BOTON_GUARDAR_CITA = Target.the("El botón guardar agendamiento de la cita").located(By.xpath("//*[@id='page-wrapper']/div/div[3]/div/a"));
	
}
