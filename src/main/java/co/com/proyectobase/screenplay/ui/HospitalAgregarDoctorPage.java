package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class HospitalAgregarDoctorPage {

	public static final Target CAMPO_NOMBRES_COMPLETOS_DOCTOR = Target.the("El campo nombres completos del doctor").located(By.id("name"));
	public static final Target CAMPO_APELLIDOS_DOCTOR = Target.the("El campo apellidos del doctor").located(By.id("last_name"));
	public static final Target CAMPO_TELEFONO_DOCTOR = Target.the("El campo teléfono del doctor").located(By.id("telephone"));
	public static final Target LISTA_TIPO_DOCUMENTO_IDENTIDAD_DOCTOR = Target.the("La lista tipo de documento de identidad del doctor").located(By.id("identification_type"));
	public static final Target CAMPO_NUMERO_DOCUMENTO_IDENTIDAD_DOCTOR = Target.the("El campo número de documento de identidad del doctor").located(By.id("identification"));
	public static final Target BOTON_GUARDAR_DOCTOR = Target.the("El botón guardar datos del doctor").located(By.xpath("//*[@id='page-wrapper']/div/div[3]/div/a"));
	public static final Target CAMPO_VERIFICACION = Target.the("El campo de verificación").located(By.xpath("//*[@id=\'page-wrapper\']/div/div[2]/div[2]/p"));
	
}
