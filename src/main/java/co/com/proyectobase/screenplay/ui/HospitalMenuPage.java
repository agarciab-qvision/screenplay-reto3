package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")

public class HospitalMenuPage extends PageObject{

		public static final Target BOTON_AGREGAR_DOCTOR = Target.the("El botón agregar doctor").located(By.xpath("//*[@id='page-wrapper']/div/div[2]/div/div/div/div/div[1]/div/a[1]"));
		public static final Target BOTON_AGREGAR_PACIENTE = Target.the("El botón agregar paciente").located(By.xpath("//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[2]"));
		
		
		public static final Target BOTON_AGENDAR_CITA = Target.the("El botón agendar cita").located(By.xpath("//*[@id='page-wrapper']/div/div[2]/div/div/div/div/div[1]/div/a[6]"));
		
}
