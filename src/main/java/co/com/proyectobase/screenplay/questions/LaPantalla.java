package co.com.proyectobase.screenplay.questions;



import co.com.proyectobase.screenplay.ui.HospitalAgregarDoctorPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaPantalla implements Question<String> {

	public static LaPantalla muestra() {
		return new LaPantalla();

	}

	@Override
	public String answeredBy(Actor actor) {
		
				return Text.of(HospitalAgregarDoctorPage.CAMPO_VERIFICACION).viewedBy(actor).asString();
	}



}
