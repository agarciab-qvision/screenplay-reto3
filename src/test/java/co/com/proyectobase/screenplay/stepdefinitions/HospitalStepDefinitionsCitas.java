package co.com.proyectobase.screenplay.stepdefinitions;

import cucumber.api.DataTable;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

import org.apache.tools.ant.filters.LineContains.Contains;
import org.hamcrest.Matcher;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaPantalla;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.DiligenciarCita;
import co.com.proyectobase.screenplay.tasks.DiligenciarDoctor;
import co.com.proyectobase.screenplay.tasks.DiligenciarPaciente;
import cucumber.api.java.Before;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Consequence;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class HospitalStepDefinitionsCitas {

	@Managed(driver="chrome")
    private WebDriver hisBrowser;
    private Actor carlos = Actor.named("Carlos");
    
    @Before
    public void configuracionInicial() {
    	carlos.can(BrowseTheWeb.with(hisBrowser));
    }
	
    @Dado("^que Carlos necesita asistir al medico$")
    public void queCarlosNecesitaAsistirAlMedico()  {
    	carlos.wasAbleTo(Abrir.LaPaginaDelHospital());
    }


    @Cuando("^el realiza el agendamiento de una Cita$")
    public void elRealizaElAgendamientoDeUnaCita(DataTable dtDatosFormulario)  {
    	List<List<String>> datos = dtDatosFormulario.raw();
    	carlos.attemptsTo(DiligenciarCita.FormularioDeRegistro(datos));
    }

    @Entonces("^el verifica que se agende la cita presentando en pantalla el mensaje Datos guardados correctamente$")
    public void elVerificaQueSeAgendeLaCitaPresentandoEnPantallaElMensajeDatosGuardadosCorrectamente() {
    	carlos.should(seeThat(LaPantalla.muestra(),equalTo("Datos guardados correctamente.")));
    }

}
